const { gql } = require('apollo-server');

module.exports = gql`
type Product {
  sort: Int
  ksort: Int
  time1: Int
  time2: Int
  id: Int
  root: Int
  kindId: Int
  subjectId: Int
  subjectParentId: Int
  name: String
  brand: String
  brandId: Int
  siteBrandId: Int
  sale: Int
  priceU: Int
  salePriceU: Int
  averagePrice: Int
  benefit: Int
  pics: Int
  rating: Int
  feedbacks: Int
  colors: [Colors]
  sizes: [Sizes]
  diffPrice: Boolean
}

type Sizes {
  name: String
  origName: String
  rank: Int
  optionId: Int
}

type Colors {
  name: String
}

input ProductInput {
  name: String
  description: String
}

input EditProductInput {
  name: String
}

type Query {
  product(ID: ID!): Product!
  getProducts(amount: Int): [Product]
}

type Mutation {
  createProduct(productInput: ProductInput): Product!
  deleteProduct(ID: ID!): Boolean
  editProduct(ID: ID!, productInput: ProductInput): Boolean
}`
