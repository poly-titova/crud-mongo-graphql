const Product = require('../models/Product');

module.exports = {
  Query: {
    async product(_, { ID }) {
      return await Product.findById(ID)
    },
    async getProducts(_, { amount }) {
      return await Product.find().sort({ createdAt: -1 }).limit(amount)
    }
  },
  Mutation: {
    async createProduct(_, {productInput: {name, description}}) {
      const createdProduct = new Product({
        name: name,
        description: description,
        createdAt: new Date().toISOString(),
        thumdsUp: 0,
        thumdsDown: 0
      });

      const res = await createdProduct.save(); // MongoDB Saving

      return {
        id: res.id,
        ...res._doc
      }
    },
    async deleteProduct(_, {ID}) {
      const wasDeleted = (await Product.deleteOne({_id: ID})).deletedCount;
      return wasDeleted; // 1 if something was deleted, 0 if nothing was deleted
    },
    async editProduct(_, {ID, productInput: {name, description}}) {
      const wasEdited = (await Product.updateOne({_id: ID}, {name: name, description: description})).modifiedCount;
      return wasEdited; // 1 if something was edited, 0 if nothing was edited
    }
  }
};
