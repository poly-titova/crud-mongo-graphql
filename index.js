const {ApolloServer} = require('apollo-server');
const mongoose = require('mongoose');

const MONGODB = "mongodb://localhost:27017/wb-products";
// const MONGODB = "mongodb+srv://admin:admin@cluster0.zdprc2g.mongodb.net/?retryWrites=true&w=majority";

// Apollo Server
// typeDefs: GraphQl Type Definitions
// resolvers: How do we resolve queries / mutations

const typeDefs = require('./graphql/typeDefs');
const resolvers = require('./graphql/resolvers');

const server = new ApolloServer({
  typeDefs,
  resolvers
});

mongoose.connect(MONGODB, {useNewUrlParser: true})
  .then(() => {
    console.log("MongoDB Connection successful");
    return server.listen({port: 5000});
  })
  .then((res) => {
    console.log(`Server running at ${res.url}`);
  });
