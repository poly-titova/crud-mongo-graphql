const { model, Schema } = require('mongoose');

const productSchema = new Schema({
  sort: Number,
  ksort: Number,
  time1: Number,
  time2: Number,
  id: Number,
  root: Number,
  kindId: Number,
  subjectId: Number,
  subjectParentId: Number,
  name: String,
  brand: String,
  brandId: Number,
  siteBrandId: Number,
  sale: Number,
  priceU: Number,
  salePriceU: Number,
  averagePrice: Number,
  benefit: Number,
  pics: Number,
  rating: Number,
  feedbacks: Number,
  colors: Array,
  sizes: Array,
  diffPrice: Boolean
});

module.exports = model('Product', productSchema);
